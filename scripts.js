// Problem 1: Complete the secondLargest function which takes in an array of numbers in input and return the second biggest number in the array. (without using sort)?
function secondLargest(array) {
    let largest;
      let max = -1;
      let place = -1
      for (let i in array){
          if(array[i] > max ){
              max = array[i]
              place = i
          }
      
      }
      //console.log(max + " " +  place)
      let secondmax = -1;
      let secondplace = -1;
      for (let i in array) {
          if(i == place){
              continue;
          }
          else{
              if(array[i] > secondmax ){
              secondmax = array[i]
              secondplace = i
          }
          }
      }
      return secondmax;
  }
  
  // Problem 2: Complete the calculateFrequency function that takes lowercase string as input and returns frequency of all english alphabet. (using only array, no in-built function)
  function calculateFrequency(string) {
    let frequency = {"1": 0,};
      //console.log(frequency.hasOwnProperty("a"))
      for (var i = 0; i < string.length; i++) {
          if (string[i] === " "){
              continue;
          }
          else if(string[i] == string[i].toUpperCase()) {
              continue;
          }
          if (frequency.hasOwnProperty(string[i])) {
              frequency[string[i]] += 1 
          }
          else {
              frequency[string[i]] = 1
          }
  
        }
      delete frequency["1"];
      console.log(frequency)
      return frequency;
  
  }
  
  // Problem 3: Complete the flatten function that takes a JS Object, returns a JS Object in flatten format (compressed)
  function flatten(unflatObject) {
      let toret = {}
      for (let i in unflatObject){
          if (typeof(unflatObject[i]) === "object"){
            
              
              newtoloop = flatten(unflatObject[i]);
              for(elements in newtoloop ){
                  keytobe = i + "." + elements
                  toret[keytobe] = newtoloop[elements];
              }
          }
          
          else{
              toret[i] = unflatObject[i];
          }
      }
      //console.log(toret)
  
      return toret;
  }
  
  // Problem 4: Complete the unflatten function that takes a JS Object, returns a JS Object in unflatten format
  function unflatten(flatObject) {
    obj = flatObject
    var result = {}, temp, substrings, property, i;
      for (property in obj) {
          substrings = property.split('.');
          temp = result;
          for (i = 0; i < substrings.length - 1; i++) {
              if (!(substrings[i] in temp)) {
                  if (isFinite(substrings[i + 1])) { 
                      temp[substrings[i]] = [];      
                  } else {
                      temp[substrings[i]] = {};                     }
              }
              temp = temp[substrings[i]];
          }
          temp[substrings[substrings.length - 1]] = obj[property];
      }
      return result;
  }
  